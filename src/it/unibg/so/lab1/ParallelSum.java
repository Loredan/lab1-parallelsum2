package it.unibg.so.lab1;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ParallelSum {
	private static int VALUES = 50;
	private static int THREADS = 10;
	
	private List<Integer> values = new ArrayList<Integer>();	
	List<SumTask> threads = new ArrayList<SumTask>();
	
	final ExecutorService pool = Executors.newFixedThreadPool(THREADS);
	
	private void readValues(Reader r) {
		Scanner in = new Scanner(r);
		while(in.hasNext())
			values.add(Integer.valueOf(in.next()));
		in.close();
	}
	
	public int parallelSum() throws InterruptedException, ExecutionException{
		int subListSize = (int)VALUES/THREADS;
		for(int i=0; i < THREADS; i++)
			threads.add(new SumTask(i, values.subList(i*subListSize, (i*subListSize)+subListSize)));
		
		List<Future<Integer>> allResults = pool.invokeAll(threads);
		
		int sum = 0;
		for (Future<Integer> partialResult: allResults) {
			int r = partialResult.get();
            System.out.println("Partial result: " + r);
            sum += r;
		}
		pool.shutdown();
		
		return sum;
	}
	

	public static void main(String[] args) {
		ParallelSum sum = new ParallelSum();
		sum.readValues(new InputStreamReader(System.in));
		
		try {
			System.out.println(sum.parallelSum());
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
	}
}
