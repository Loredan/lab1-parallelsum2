# SOMMA PARALLELA THREAD POOL #

Scrivere un programma multithread in Java che dato un array di 50 valori numerici ne esegue la somma in modo parallelo.
Creare  10  thread  e assegnare ad ogni thread una partizione dell’array (5 valori).
Ogni thread esegue la somma dei propri valori e salva il risultato internamente.
Il thread main deve sincronizzarsi sulla terminazione dei thread figli dopodiche' somma i risultati parziali e li stampa su stdout.

Svolgere l’esercizio utilizzando un thread pool:

```
#!java
final ExecutorService pool = Executors.newFixedThreadPool(10);
```
I Thread che eseguono la somma in parallelo devono implementare l’interfaccia:
```
#!java
Callable<V>
```
Il main thread accumula i risultati parziali in una lista di oggetti di tipo:
```
#!java
Future<V>
```
La somma totale viene stampata su stdout.